import email
import os
import re


def fileRead(path):
    for email_file in os.scandir(path + '\\unprocessed'):
        with open(email_file, 'rb+') as file:
            processEmail(email.message_from_binary_file(file))
            file.close()
            os.rename(file.name, file.name + ".processed")


def processEmail(email_content):
    for part in email_content.walk():
        if part.get_content_type() == 'text/plain':
            client = parse_client(email_content['to'])
            subject = email_content['subject']
            body = part.get_payload(decode=True)
            msgid = parse_msgid(body)

            write_file_path = os.path.join(base_path, "processed", client)
            os.makedirs(write_file_path, exist_ok=True)
            write_file = open(write_file_path + '\\' + msgid + ".txt", "w")

            write_file.write('Subject: ' + subject + '\n\n---\n\n')
            write_file.write(body.decode())
            write_file.close()


def parse_msgid(message_body):
    msgid = re.search(b'(?<=Ref:).*', message_body, re.M).group().decode()
    return msgid


def parse_client(email_address):
    client_list = {
        'loop.com': 'Venture',
        'tripsmarter.com': 'BeachTV',
        'lesea.com': 'LeSEA',
        'gray.tv': 'Gray',
        'entravision.com': 'Entravision'
    }

    domain = email_address.split('@')[1][:-1]
    return client_list.get(domain)


def main():
    True


base_path = os.path.join(os.path.dirname(__file__), "emails")
fileRead(base_path)
