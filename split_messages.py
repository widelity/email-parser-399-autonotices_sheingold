import sys
import os
import email


def fileRead(path):
    for email_file in os.scandir(path + '\\unprocessed'):
        with open(email_file, 'r+') as file:
            processBody(file)
            file.close()
            os.rename(file.name, file.name + ".processed")


def processBody(email_text):
    write_file_path = os.path.join(base_path, "split")
    os.makedirs(write_file_path, exist_ok=True)

    part_counter = 1
    email_string = ""
    for line in email_text:
        if "> ---------- Forwarded message ----------" in line:
            with open(email_text.name + '_' + str(part_counter), 'w') /
            as write_file:
                write_file.write(email_string)
            part_counter += 1
            email_string = ""
        else:
            email_string += line

#            write_file_path = os.path.join(base_path, "processed", client)
#            os.makedirs(write_file_path, exist_ok=True)
#            write_file = open(write_file_path + '\\' + msgid + ".txt", "w")

#            write_file.write('Subject: ' + subject + '\n\n---\n\n')
#            write_file.write(body.decode())
#            write_file.close()


def parse_msgid(message_body):
    msgid = re.search(b'(?<=Ref:).*', message_body, re.M).group().decode()
    return msgid


base_path = os.path.join(os.path.dirname(__file__), "emails")
fileRead(base_path)
